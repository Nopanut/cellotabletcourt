package net.pirsquare.qhappy.QhappyServer.ATV;

import android.content.Context;
import android.util.Log;

import net.pirsquare.db.SharePreferenceUtil;
import net.pirsquare.qhappy.QhappyServer.ATV.model.BlockModel;
import net.pirsquare.qhappy.QhappyServer.SocketService.ServerSocket;
import net.pirsquare.qhappy.QhappyServer.StatusBar.StatusBarController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Michael on 07/10/2014.
 */
public class ATVController {
    private static ATVController mInstance;

    public static boolean isFiFo = false;
    private Context mContext;

    public static ATVController instance(Context mContext, ServerSocket socket, String room) {
        if (mInstance == null) {
            return mInstance = new ATVController(mContext, socket, room);
        } else {
            return mInstance;
        }
    }

    private ArrayList<BlockModel> _last_call_block_temp = new ArrayList<BlockModel>();
    private ArrayList<BlockModel> _last_call_block = new ArrayList<BlockModel>();

    private ArrayList<BlockModel> _last_call_block_0_temp = new ArrayList<BlockModel>();
    private ArrayList<BlockModel> _last_call_block_0 = new ArrayList<BlockModel>();

    private ArrayList<BlockModel> _last_call_block_1_temp = new ArrayList<BlockModel>();
    private ArrayList<BlockModel> _last_call_block_1 = new ArrayList<BlockModel>();

    private HashMap<String, BlockModel> last_group_block = new HashMap<String, BlockModel>();

    private ServerSocket mServerSocket;
    private String mRoom;
    private SharePreferenceUtil shareObject;

    private JSONObject _seat_map = new JSONObject();

    public ATVController(Context mContent, ServerSocket socket, String room) {

        this.mContext = mContent;
        shareObject = SharePreferenceUtil.instance(mContent);

        reloadDataFromSharePreferenceUtil();

        mServerSocket = socket;
        mRoom = room;
    }

    // private String lastCall = "";

    // public void deliver() {
    // new Thread() {
    // @Override
    // public void run() {
    // try {
    // JSONObject json_object = new JSONObject();
    // json_object.put("f", "deliver");
    // JSONArray json_object_deliver_array = new JSONArray();
    //
    // for (String temp : deliver_array) {
    // json_object_deliver_array.put(temp);
    // }
    //
    // json_object.put("deliver", json_object_deliver_array);
    //
    // String message = json_object.toString();
    //
    // fifo_message.add(message);
    // sendRemaining();
    //
    // } catch (JSONException e) {
    // e.printStackTrace();
    // }
    // }
    // }.start();
    // }

    private void validateMovement(String group_id, int queue_id,
                                  int counter_number, ArrayList<BlockModel> last_call_block,
                                  ArrayList<BlockModel> last_call_block_temp) {

        last_call_block_temp.clear();
        // add to temp first
        int len = last_call_block.size();
        for (int i = 0; i < len; i++) {
            last_call_block_temp.add(last_call_block.get(i));
        }

        last_call_block.clear();

        // move same to first if found
        boolean is_require_new_block = true;
        len = last_call_block_temp.size();
        for (int i = 0; i < len; i++) {
            boolean isSame = last_call_block_temp.get(i).isSame(group_id,
                    queue_id, counter_number);

            if (isSame) {
                // add to first
                last_call_block.add(last_call_block_temp.remove(i));
                is_require_new_block = false;
                break;
            }
        }

        BlockModel newBlock = null;

        if (is_require_new_block) {
            newBlock = new BlockModel();
            newBlock.set(group_id, queue_id, counter_number);
            // add to first
            last_call_block.add(newBlock);
        } else {

        }

        // add the rest
        while (last_call_block_temp.size() > 0) {
            last_call_block.add(last_call_block_temp.remove(0));
        }

        // remove extra?
        while (last_call_block.size() > 6) {
            last_call_block.remove(last_call_block.size() - 1);
        }
    }

    public void toggle() {
        JSONObject json_object = new JSONObject();

        try {
            json_object.put("f", "toggle");
            json_object.put("rid", new Date().toString());

        } catch (Exception e) {
            e.printStackTrace();
        }

        String message = json_object.toString();

        if (isFiFo) {
            fifo_message.add(message);
            sendRemaining();
        } else {
            send(message);
        }
    }

    public void call(final String qt, final String group_id,
                     final int queue_id, final int counter_number,
                     final String request_id, final String seat_text, final String color) {
        new Thread() {
            @Override
            public void run() {

                Log.i("ANE", "queue = " + group_id + queue_id + " counter = "
                        + counter_number + " seat = " + seat_text);

                // not same do shift
                validateMovement(group_id, queue_id, counter_number,
                        _last_call_block, _last_call_block_temp);

                if (counter_number == 0)
                    validateMovement(group_id, queue_id, counter_number,
                            _last_call_block_0, _last_call_block_0_temp);
                else if (counter_number == 1)
                    validateMovement(group_id, queue_id, counter_number,
                            _last_call_block_1, _last_call_block_1_temp);

                // 1 = go in shop
                // 0 = go order/delivery

                // set last group
                if (counter_number == 1) {
                    BlockModel localBlockModel = (BlockModel) _last_call_block
                            .get(0);
                    String group_id = localBlockModel.get_group_id();
                    if (last_group_block.containsKey(group_id)) {

                        BlockModel prevBlock = last_group_block.get(group_id);

                        // if current is more than prev
                        if (localBlockModel.get_queue_id() > prevBlock
                                .get_queue_id()) {
                            last_group_block.put(group_id, localBlockModel);
                            saveLastGroup();
                        }

                    } else {
                        // first of the group
                        last_group_block.put(group_id, localBlockModel);
                        saveLastGroup();
                    }
                }

                // calculate all last group
                // sort key
                JSONArray json_array_last_group = new JSONArray();
                List<String> sortedKeys = new ArrayList<String>(
                        last_group_block.keySet());
                Collections.sort(sortedKeys);

                for (String key : sortedKeys) {
                    BlockModel model = last_group_block.get(key);
                    json_array_last_group.put(model.toLastGroup());
                }

                // calculate last queue/ global history
                // last queue
                JSONArray json_array_last_queue = new JSONArray();
                int len = _last_call_block.size();
                for (int i = 0; i < len; i++) {
                    json_array_last_queue.put(_last_call_block.get(i)
                            .toLastQueue());
                }

                // calculate last queue/ type 0
                // last queue
                JSONArray json_array_last_queue_0 = new JSONArray();
                int len0 = _last_call_block_0.size();
                for (int i = 0; i < len0; i++) {
                    json_array_last_queue_0.put(_last_call_block_0.get(i)
                            .toLastQueue());
                }

                // calculate last queue/ type 1
                // last queue
                JSONArray json_array_last_queue_1 = new JSONArray();
                int len1 = _last_call_block_1.size();
                for (int i = 0; i < len1; i++) {
                    json_array_last_queue_1.put(_last_call_block_1.get(i)
                            .toLastQueue());
                }

                JSONObject json_object = new JSONObject();

                try {
                    json_object.put("f", "call");
                    json_object.put("rid", request_id);

                    json_object.put("last_queue", json_array_last_queue);
                    json_object.put("last_queue_0", json_array_last_queue_0);
                    json_object.put("last_queue_1", json_array_last_queue_1);

                    json_object.put("last_group", json_array_last_group);

                    json_object.put("qt", qt);
                    json_object.put("seat_text", seat_text);
                    json_object.put("color", color);
                    json_object.put("group_count", _group_count);

                } catch (Exception e) {
                    e.printStackTrace();
                }

                String message = json_object.toString();
                Log.e("Message", ">>>>>>>>>>>>>> \n " + message + "\n<<<<<<<<<<<<<<<");
                if (isFiFo) {
                    fifo_message.add(message);
                    sendRemaining();
                } else {
                    send(message);
                }

                saveHistory(_last_call_block, "_last_call_block");
                saveHistory(_last_call_block_0, "_last_call_block_0");
                saveHistory(_last_call_block_1, "_last_call_block_1");

                // lastCall = call;
            }
        }.start();
    }

    private void saveLastGroup() {
        Log.i("ANE", "saveLastGroup ");
        try {
            ArrayList<String> save_data = new ArrayList<String>();
            for (Map.Entry<String, BlockModel> entry : last_group_block
                    .entrySet()) {
                BlockModel block = (BlockModel) entry.getValue();

                save_data.add(block.toString());
            }
            this.shareObject.put("last_group_block", save_data);
        } catch (Exception e) {
            Log.i("ANE", "saveLastGroup_error " + e.toString());
        }
    }

    private void saveHistory(ArrayList<BlockModel> ary, String key) {
        Log.i("ANE", "save" + key);
        try {
            ArrayList<String> save_data = new ArrayList<String>();
            for (BlockModel block : ary) {
                save_data.add(block.toString());
            }

            Log.i("ANE", "saveHistory " + key + "  " + save_data);

            this.shareObject.put(key, save_data);
        } catch (Exception e) {
            Log.i("ANE", "saveHistory_error " + key + "  " + e.toString());
        }
    }

    private final ArrayList<String> fifo_message = new ArrayList<String>();

    public void sendRemaining() {
        if (mServerSocket.countInChannel(mRoom) <= 0)
            return;

        while (!fifo_message.isEmpty()) {
            if (StatusBarController.isEnable(StatusBarController.ID_TV))
                mServerSocket.sendToChannel(fifo_message.remove(0), mRoom);
            else
                break;
        }
    }

    public void send(String message) {
        mServerSocket.sendToChannel(message, mRoom);
    }

    private ArrayList<String> new_queue_list = new ArrayList<String>();

    public void newQueue(String queue_name) {
        if (mServerSocket.countInChannel(mRoom) <= 0)
            return;

        Log.i("ANE", "newQueue " + queue_name);

        if (!new_queue_list.contains(queue_name)) {

            new_queue_list.add(0, queue_name);

            shareObject.put("new_queue_list", new_queue_list);

            // limit array to 5
            while (new_queue_list.size() > 6) {
                new_queue_list.remove(new_queue_list.size() - 1);
            }

            sendNewQueueUpdate();

        } else {
            Log.i("ANE", "newQueue ! already contain " + queue_name);
        }
    }

    // remove found queue from list
    public void removeNewQueue(String queue_name) {
        if (mServerSocket.countInChannel(mRoom) <= 0)
            return;

        Log.i("ANE", "removeNewQueue " + queue_name);

        try {
            if (new_queue_list.contains(queue_name)) {

                new_queue_list.remove(queue_name);

                shareObject.put("new_queue_list", new_queue_list);

                sendNewQueueUpdate();

            }
        } catch (Exception e) {
            Log.i("ANE", "removeNewQueue ! already not contain " + queue_name);
        }
    }

    private void sendNewQueueUpdate() {
        Log.i("ANE", "sendNewQueueUpdate ");

        // ////sent to server
        JSONArray json_object_new_queue_array = new JSONArray();

        for (String newQueueModel : new_queue_list) {
            json_object_new_queue_array.put(newQueueModel);
        }

        JSONObject json_object = new JSONObject();

        try {
            json_object.put("f", "newQueue");
            json_object.put("new_queue_list", json_object_new_queue_array);

        } catch (Exception e) {
            Log.i("ANE", "sendNewQueueUpdate_error " + e.toString());
        }

        String message = json_object.toString();

        if (isFiFo) {
            fifo_message.add(message);
            sendRemaining();
        } else {
            send(message);
        }
    }

    private ArrayList<String> deliver_queue_list = new ArrayList<String>();

    public void removeDeliver(String queue_name) {
        if (mServerSocket.countInChannel(mRoom) <= 0)
            return;

        Log.i("ANE", "removeDeliver " + queue_name);

        try {
            if (deliver_queue_list.contains(queue_name)) {

                deliver_queue_list.remove(queue_name);

                shareObject.put("deliver_queue_list", deliver_queue_list);

                sendDeliverUpdate();

            } else {
                Log.i("ANE", "removeDeliver ! already not contain "
                        + queue_name);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addDeliver(String queue_name) {
        if (mServerSocket.countInChannel(mRoom) <= 0)
            return;

        Log.i("ANE", "addDeliver " + queue_name);

        try {
            // not found then add
            if (!deliver_queue_list.contains(queue_name)) {

                deliver_queue_list.add(0, queue_name);

                shareObject.put("deliver_queue_list", deliver_queue_list);

            } else {

                // remove current then add to front
                deliver_queue_list.remove(queue_name);
                deliver_queue_list.add(0, queue_name);

            }

            // limit array to 6
            while (deliver_queue_list.size() > 6) {
                deliver_queue_list.remove(deliver_queue_list.size() - 1);
            }

            sendDeliverUpdate();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendDeliverUpdate() {
        Log.i("ANE", "sendDeliverUpdate ");

        // send to tv
        JSONArray json_object_deliver_array = new JSONArray();
        for (String temp : deliver_queue_list) {
            json_object_deliver_array.put(temp);
        }

        JSONObject json_object = new JSONObject();

        try {
            json_object.put("f", "deliver");
            json_object.put("deliver", json_object_deliver_array);

            String message = json_object.toString();

            if (isFiFo) {
                fifo_message.add(message);
                sendRemaining();
            } else {
                send(message);
            }
        } catch (Exception e) {
            Log.i("ANE", "sendDeliverUpdate_error " + e.toString());
        }
    }

    // send both
    public void sendAllUpdate() {
        if (mServerSocket.countInChannel(mRoom) <= 0)
            return;

        Log.i("ANE", "sendAllUpdate ");

        // send to tv
        JSONArray json_object_deliver_array = new JSONArray();
        for (String temp : deliver_queue_list) {
            json_object_deliver_array.put(temp);
        }

        JSONObject json_object = new JSONObject();
        JSONArray json_array_last_group = new JSONArray();

        // //sent to server
        JSONArray json_object_new_queue_array = new JSONArray();
        for (String newQueueModel : new_queue_list) {
            json_object_new_queue_array.put(newQueueModel);
        }

        // last group
        List<String> sortedKeys = new ArrayList<String>(
                last_group_block.keySet());
        Collections.sort(sortedKeys);
        for (String key : sortedKeys) {
            BlockModel model = (BlockModel) this.last_group_block.get(key);
            json_array_last_group.put(model.toLastGroup());
        }

        // calculate last queue/ global history
        // last queue
        JSONArray json_array_last_queue = new JSONArray();
        int len = _last_call_block.size();
        for (int i = 0; i < len; i++) {
            json_array_last_queue.put(_last_call_block.get(i).toLastQueue());
        }

        // calculate last queue/ type 0
        // last queue
        JSONArray json_array_last_queue_0 = new JSONArray();
        int len0 = _last_call_block_0.size();
        for (int i = 0; i < len0; i++) {
            json_array_last_queue_0
                    .put(_last_call_block_0.get(i).toLastQueue());
        }

        // calculate last queue/ type 1
        // last queue
        JSONArray json_array_last_queue_1 = new JSONArray();
        int len1 = _last_call_block_1.size();
        for (int i = 0; i < len1; i++) {
            json_array_last_queue_1
                    .put(_last_call_block_1.get(i).toLastQueue());
        }

        try {
            json_object.put("f", "update");
            json_object.put("deliver", json_object_deliver_array);
            json_object.put("new_queue_list", json_object_new_queue_array);
            json_object.put("last_group", json_array_last_group);
            json_object.put("group_count", _group_count);

            _seat_map.put("A", "1-4");
            _seat_map.put("B", "5-8");
            json_object.put("seat_map", _seat_map);

            json_object.put("last_queue", json_array_last_queue);
            json_object.put("last_queue_0", json_array_last_queue_0);
            json_object.put("last_queue_1", json_array_last_queue_1);

        } catch (Exception e) {
            Log.i("ANE", "sendAllUpdate_error " + e.toString());
        }

        String message = json_object.toString();

        if (isFiFo) {
            fifo_message.add(message);
            sendRemaining();
        } else {
            send(message);
        }
    }

    public void clearData() {
        Log.i("ANE", "clearData ");

        shareObject.clear();
        _group_count = 0;
        _last_call_block = new ArrayList<BlockModel>();
        _last_call_block_0 = new ArrayList<BlockModel>();
        _last_call_block_1 = new ArrayList<BlockModel>();
        // _seat_map = new JSONObject();

        reloadDataFromSharePreferenceUtil();

        JSONObject json_object = new JSONObject();

        try {
            json_object.put("f", "clearData");
        } catch (Exception e) {
            Log.i("ANE", "clearData_error " + e.toString());
        }

        String message = json_object.toString();

        if (isFiFo) {
            fifo_message.add(message);
            sendRemaining();
        } else {
            send(message);
        }

    }

    private void reloadDataFromSharePreferenceUtil() {
        try {
            Log.i("ANE", "reloadDataFromSharePreferenceUtil ");

            new_queue_list = shareObject.getArrayList("new_queue_list");
            deliver_queue_list = shareObject.getArrayList("deliver_queue_list");

            if (new_queue_list == null)
                new_queue_list = new ArrayList<String>();

            if (deliver_queue_list == null)
                deliver_queue_list = new ArrayList<String>();

            ArrayList<String> last_group_raw = shareObject
                    .getArrayList("last_group_block");
            if (last_group_raw == null) {
                last_group_block = new HashMap<String, BlockModel>();
            } else {
                for (String s : last_group_raw) {

                    BlockModel blockModel = BlockModel.fromString(s);
                    last_group_block.put(blockModel.get_group_id(), blockModel);
                }
            }

            reloadHistoryFromSave(_last_call_block, "_last_call_block");
            reloadHistoryFromSave(_last_call_block_0, "_last_call_block_0");
            reloadHistoryFromSave(_last_call_block_1, "_last_call_block_1");

        } catch (Exception e) {
            Log.i("ANE",
                    "reloadDataFromSharePreferenceUtil_error " + e.toString());
        }
    }

    private void reloadHistoryFromSave(ArrayList<BlockModel> block, String key) {
        ArrayList<String> raw = shareObject.getArrayList(key);

        Log.i("ANE", "reloadHistoryFromSave ");

        if (raw == null || raw.size() == 0) {

            Log.i("ANE", "reloadHistoryFromSave null " + key);
        } else {

            Log.i("ANE", "reloadHistoryFromSave load " + " len: " + raw.size()
                    + key + " val: " + raw);

            try {
                for (String s : raw) {

                    // Log.i("ANE", "reloadHistoryFromSave loadss|" + s + "|");

                    BlockModel blockModel = BlockModel.fromString(s);
                    block.add(blockModel);
                }
            } catch (Exception e) {
            }
        }
    }

    private int _group_count = 1;

    // public void setGroupCount(int asInt) {
    // group_count = asInt;
    //
    // JSONObject json_object = new JSONObject();
    // try{
    // json_object.put("group_count", group_count);
    // }catch(Exception e){
    // Log.i("ANE", "clearData_error " + e.toString());
    // }
    //
    // String message = json_object.toString();
    //
    // if (isFiFo) {
    // fifo_message.add(message);
    // sendRemaining();
    // } else {
    // send(message);
    // }
    // }

    public void setSeatMap(String seat_map, boolean sendUpdate) {

        JSONObject seat_map_json;

        try {

            seat_map_json = new JSONObject(seat_map);
            _group_count = seat_map_json.length();

            _seat_map = seat_map_json;

        } catch (JSONException e1) {
            e1.printStackTrace();
        }

        if (mServerSocket.countInChannel(mRoom) <= 0)
            return;

        if (!sendUpdate)
            return;

        // ///////////////////////////////////////////
        JSONObject json_object = new JSONObject();
        try {
            json_object.put("group_count", _group_count);
            json_object.put("seat_map", _seat_map);

        } catch (Exception e) {

            Log.i("ANE", "setSeatMap " + e.toString());

        }

        String message = json_object.toString();

        if (isFiFo) {
            fifo_message.add(message);
            sendRemaining();
        } else {
            send(message);
        }
    }
}
