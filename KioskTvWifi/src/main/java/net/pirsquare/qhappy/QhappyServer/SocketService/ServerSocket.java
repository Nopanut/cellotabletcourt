package net.pirsquare.qhappy.QhappyServer.SocketService;

import android.util.Log;

import org.java_websocket_local.WebSocket;
import org.java_websocket_local.drafts.Draft;
import org.java_websocket_local.handshake.ClientHandshake;
import org.java_websocket_local.server.WebSocketServer;

import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.nio.channels.SelectionKey;
import java.util.Collection;
import java.util.Collections;

public class ServerSocket
        extends WebSocketServer {
    public ServerSocket(int port, Draft d, int thread_count)
            throws UnknownHostException {
        super(new InetSocketAddress(port), thread_count, Collections.singletonList(d));
    }

    public void onOpen(WebSocket conn, ClientHandshake handshake) {
    }

    public void onError(WebSocket conn, Exception ex) {
    }

    public void onClose(WebSocket conn, int code, String reason, boolean remote) {
    }

    public void onMessage(WebSocket conn, String message) {
    }

    protected boolean onConnect(SelectionKey key) {
        return super.onConnect(key);
    }

    public void onClosing(WebSocket conn, int code, String reason, boolean remote) {
        super.onClosing(conn, code, reason, remote);
    }

    public void sendToChannel(String text, String room) {
        Log.i("ANE", "sendToChannel = " + text);

        Collection<WebSocket> con = connections();
        synchronized (con) {
            for (WebSocket c : con) {
                if (c.getRoom().equals(room)) {
                    c.send(text);
                }
            }
        }
    }

    public int countInChannel(String room) {
        int count = 0;
        Collection<WebSocket> con = connections();
        for (WebSocket c : con) {
            if (c.getRoom().equals(room)) {
                count++;
            }
        }
        return count;
    }

    public void sendToAll(String text) {
        Collection<WebSocket> con = connections();
        synchronized (con) {
            for (WebSocket c : con) {
                c.send(text);
            }
        }
    }
}
