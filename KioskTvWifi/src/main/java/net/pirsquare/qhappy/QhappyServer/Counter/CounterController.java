package net.pirsquare.qhappy.QhappyServer.Counter;

import android.content.Context;
import android.os.Handler;
import android.util.Log;

import net.pirsquare.qhappy.QhappyServer.ATV.ATVController;
import net.pirsquare.qhappy.QhappyServer.QueueMachine.QueueMachineController;
import net.pirsquare.qhappy.QhappyServer.SocketService.ServerSocket;

import org.java_websocket_local.WebSocket;
import org.java_websocket_local.drafts.Draft_17;
import org.java_websocket_local.handshake.ClientHandshake;
import org.json.JSONObject;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Calendar;

public class CounterController {
    public static String SOCKET_CHANNEL_TV = "TV";
    public static String SOCKET_CHANNEL_QMACHINE = "QUEUEMACHINE";
    public static String SOCKET_CHANNEL_COUNTER = "counter";
    private static String TAG = "CounterController";
    private static ServerSocket mServerSocket;
    private ATVController mATVController;
    private QueueMachineController mQueueMachineController;
    private TVCallbackListener callbackListener;
    private long last_tv_ping = 0L;
    private long last_QMachine_ping = 0L;

    private Context mContext;
    private String macAddress;

    public void callBack(String code, String level) {
        // override outside
    }

    public void callbackWithMacAddress(String code, String level, String mac) {

    }

    public CounterController(Context mContext) {
        this.mContext = mContext;
        initSocket();
    }

    public void setCallbackListener(TVCallbackListener callbackListener) {
        this.callbackListener = callbackListener;
    }


    // public void setGroupCount(int asInt) {
    // mATVController.setGroupCount(asInt);
    // }

    public void callQueue(String qt, String group_id, int queue_id,
                          int counter_id, String seat_text, String color) {
        mATVController.call(qt, group_id, queue_id, counter_id, now() + "",
                seat_text, color);
    }

    public void newQueue(String queue_name) {
        mATVController.newQueue(queue_name);
    }

    // remove found queue from list
    public void removeNewQueue(String queue_name) {
        mATVController.removeNewQueue(queue_name);
    }

    public void addDeliver(String queue_name) {
        mATVController.addDeliver(queue_name);
    }

    // remove found queue from deliver list
    public void removeDeliver(String queue_name) {
        mATVController.removeDeliver(queue_name);
    }

    // TODO seperate init services
    public void setSeatMap(String seat_map, boolean sendUpdate) {
        mATVController.setSeatMap(seat_map, sendUpdate);

        mQueueMachineController.setSeatMap(seat_map, true);
    }

    public void toggle() {
        mATVController.toggle();
    }

    public void sendAllUpdate(String channel) {

        if (channel.equals(SOCKET_CHANNEL_TV))
            mATVController.sendAllUpdate();
        else if (channel.equals(SOCKET_CHANNEL_QMACHINE))
            mQueueMachineController.sendAllUpdate();

    }

    public void sendAllUpdate() {
        sendAllUpdate(SOCKET_CHANNEL_TV);
    }

    public void clearData() {
        mATVController.clearData();
    }

    private void initSocket() {
        try {
            mServerSocket = new ServerSocket(31400, new Draft_17(), 9) {
                public void onClose(WebSocket conn, int code, String reason,
                                    boolean remote) {
                    if (conn.getRoom().equals(
                            CounterController.SOCKET_CHANNEL_TV)) {
                        callBack("disableLED",
                                CounterController.SOCKET_CHANNEL_TV);
                    }
                    callbackListener.onDisconnect();
                    super.onClose(conn, code, reason, remote);
                }

                public void onMessage(WebSocket conn, String message) {
                    callbackListener.onConnect(macAddress);
                    Log.i(CounterController.TAG, "onMessage " + message);
                    try {
                        JSONObject messageObject = new JSONObject(message);
                        String functionName = messageObject.getString("f");
                        if (messageObject.has("cid")) {
                            conn.setRoom(CounterController.SOCKET_CHANNEL_COUNTER);
                        }

                        if (messageObject.has("tid")) {
                            conn.setRoom(CounterController.SOCKET_CHANNEL_TV);
                        }
                        if (messageObject.has("ma")) {
                            macAddress = messageObject.getString("ma");
                            callBack("MacAddress", messageObject.getString("ma"));
                        }

                        if (functionName.equals("ping")) {
                            conn.send(message);
                            if (conn.getRoom().equals(
                                    CounterController.SOCKET_CHANNEL_TV)) {
                                // if(now() > last_tv_ping + 5000L){
                                // conn.close();
                                // }

                                last_tv_ping = now();

                                conn.send(message);
                            }
                            if (conn.getRoom().equals(
                                    CounterController.SOCKET_CHANNEL_QMACHINE)) {
                                // if(now() > last_tv_ping + 5000L){
                                // conn.close();
                                // }

                                last_QMachine_ping = now();

                                conn.send(message);
                            }
                        } else if (functionName.equals("mirror")) {
                            String ret = message.substring(0,
                                    message.length() - 1)
                                    + ",\"st\":"
                                    + CounterController.this.now() + "}";
                            CounterController.mServerSocket.sendToChannel(ret,
                                    CounterController.SOCKET_CHANNEL_COUNTER);// TODO
                            // mirror
                            // own
                            // channel
                        } else if (functionName.equals("changeChannel")) {
                            String channel = messageObject.getString("c");
                            conn.setRoom(channel);
                            if (!channel
                                    .equals(CounterController.SOCKET_CHANNEL_TV)) {
                                try {
                                    JSONObject json = new JSONObject();
                                    json.put("f", "changeChannel");
                                    json.put("r", 1);

                                    String ret = json.toString();
                                    conn.send(ret);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            if (channel
                                    .equals(CounterController.SOCKET_CHANNEL_TV)) {
                                last_tv_ping = now();
                            } else if (channel
                                    .equals(CounterController.SOCKET_CHANNEL_QMACHINE)) {
                                last_QMachine_ping = now();
                            }

                            callBack("enableLED", channel);

                        } else if (functionName.equals("called")) {

                            callBack("called", message);

                        } /*
                         * else if ((functionName.equals("accepted")) ||
						 * (functionName.equals("call"))) {
						 * CounterController.this.callBack("call", message); }
						 */ else if (functionName.equals("requestUpdate")) {// TV

                            sendAllUpdate(conn.getRoom());

                        } else if (functionName.equals("addQueue")) {

                            String group_id = messageObject.getString("g");

                            callBack("addQueue", group_id);// TO FLASH AS3

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                public void onOpen(WebSocket conn, ClientHandshake handshake) {
                    Log.i(CounterController.TAG, "onOpen");

                }
            };
            mServerSocket.start();

            // init services
            initServices();

        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        final Handler h = new Handler();
        Runnable r = new Runnable() {
            public void run() {

                // tv
                if (now() > last_tv_ping + 5000L) {
                    if (!lastCallBackTV.equals("disableLED"
                            + CounterController.SOCKET_CHANNEL_TV)) {
                        callBack("disableLED",
                                CounterController.SOCKET_CHANNEL_TV);
                        lastCallBackTV = ("disableLED" + CounterController.SOCKET_CHANNEL_TV);
                    }
                } else if (!lastCallBackTV.equals("enableLED"
                        + CounterController.SOCKET_CHANNEL_TV)) {
                    callbackWithMacAddress("enableLED", CounterController.SOCKET_CHANNEL_TV, macAddress);


                    ATVController.instance(mContext, CounterController.mServerSocket,
                            CounterController.SOCKET_CHANNEL_TV)
                            .sendRemaining();

                    lastCallBackTV = ("enableLED" + CounterController.SOCKET_CHANNEL_TV);
                }

                // queueMachine
                if (now() > last_QMachine_ping + 5000L) {

                    if (!lastCallBackQMACHINE.equals("disableLED"
                            + CounterController.SOCKET_CHANNEL_QMACHINE)) {

                        callBack("disableLED",
                                CounterController.SOCKET_CHANNEL_QMACHINE);

                        lastCallBackQMACHINE = ("disableLED" + CounterController.SOCKET_CHANNEL_QMACHINE);
                    }

                } else if (!lastCallBackQMACHINE.equals("enableLED"
                        + CounterController.SOCKET_CHANNEL_QMACHINE)) {

                    callBack("enableLED",
                            CounterController.SOCKET_CHANNEL_QMACHINE);

                    QueueMachineController.instance(
                            CounterController.mServerSocket,
                            CounterController.SOCKET_CHANNEL_QMACHINE)
                            .sendRemaining();

                    lastCallBackQMACHINE = ("enableLED" + CounterController.SOCKET_CHANNEL_QMACHINE);
                }

                h.postDelayed(this, 2000L);
            }
        };
        r.run();
    }

    private void initServices() {
        this.mATVController = ATVController.instance(mContext, mServerSocket,
                SOCKET_CHANNEL_TV);
        this.mQueueMachineController = QueueMachineController.instance(
                mServerSocket, SOCKET_CHANNEL_QMACHINE);
    }

    public void disConnect() {
        try {
            mServerSocket.stop();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private String lastCallBackTV = "";
    private String lastCallBackQMACHINE = "";

    private long now() {
        Calendar now = Calendar.getInstance();
        return now.getTimeInMillis();
    }

    public interface TVCallbackListener {
        void onConnect(String macAddress);

        void onDisconnect();
    }
}