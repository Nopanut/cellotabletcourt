package net.pirsquare.qhappy.QhappyServer.StatusBar;

import java.util.HashMap;

/**
 * Created by Michael on 29/10/2014.
 */
public class StatusBarController {

    public static final int ID_INTERNET = 0;
    public static final int ID_PARSE = 1;
    public static final int ID_TV = 2;
    public static final int ID_PRINTER = 3;

    private static boolean init = false;

    public static boolean hasInit() {
        return init;
    }

    private static StatusBarController mInstance;

    public static StatusBarController instance() {
        if (mInstance == null)
            return mInstance = new StatusBarController();
        else {
            
            return mInstance;
        }
    }

    public static boolean isInit() {
        return init;
    }

    public static HashMap<Integer, Integer> getLed_map() {
        return led_map;
    }

    private static HashMap<Integer, Integer> led_map = new HashMap<Integer, Integer>();

    public static void setLEDColor(final int id, final int color) {

        led_map.put(id, color);
    }

    //enable 4bec13 green
    public static void enableLED(int id) {
        setLEDColor(id, 0xff4bec13);
    }

    //disable DF0101 red
    public static void disableLED(int id) {
        setLEDColor(id, 0xffDF0101);
    }

    public static boolean isEnable(int id) {
        return led_map.get(id) == 0xff4bec13;
    }
}
