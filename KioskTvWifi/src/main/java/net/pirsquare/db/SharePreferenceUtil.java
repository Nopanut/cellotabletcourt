package net.pirsquare.db;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Michael on 19/08/2015.
 */
public class SharePreferenceUtil {

    private static SharePreferenceUtil _INSTANCE;

    public static SharePreferenceUtil instance(Context c) {
        if (_INSTANCE != null)
            return _INSTANCE;

        return _INSTANCE = new SharePreferenceUtil(c);
    }


    private SharedPreferences mSharedPreferences;
    private SharedPreferences.Editor mEditor;

    public SharePreferenceUtil(Context c) {
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(c);
        mEditor = mSharedPreferences.edit();
    }

    public void clear(){
    	mEditor.clear();
        mEditor.commit();
    }
    
    ////////////////////////////////////////////////////////put
    public void put(String key, Boolean v) {
        mEditor.putBoolean(key, v);
        mEditor.commit();
    }

    public void put(String key, float v) {
        mEditor.putFloat(key, v);
        mEditor.commit();
    }

    public void put(String key, int v) {
        mEditor.putInt(key, v);
        mEditor.commit();
    }

    public void put(String key, long v) {
        mEditor.putLong(key, v);
        mEditor.commit();

    }

    public void put(String key, String v) {
        mEditor.putString(key, v);
        mEditor.commit();
    }

    public void put(String key, Set<String> v) {
        mEditor.putStringSet(key, v);
        mEditor.commit();
    }

    public void put(String key, List<String> v) {
        mEditor.putString(key, v.toString());
        mEditor.commit();
    }

    public void put(String key, ArrayList<String> v) {
        mEditor.putString(key, v.toString());
        mEditor.commit();
    }


    ///////////////////////////////////////////////////GET
    public Map<String, ?> getAll() {
        return mSharedPreferences.getAll();
    }

    public String getString(String s, String s2) {
        return mSharedPreferences.getString(s, s2);
    }

    public Set<String> getStringSet(String s) {
        return mSharedPreferences.getStringSet(s, null);
    }

    public int getInt(String s) {
        return mSharedPreferences.getInt(s, 0);
    }

    public long getLong(String s) {
        return mSharedPreferences.getLong(s, 0);
    }

    public float getFloat(String s) {
        return mSharedPreferences.getFloat(s, 0);
    }

    public boolean getBoolean(String s) {
        return mSharedPreferences.getBoolean(s, false);
    }

    public ArrayList<String> getArrayList(String s) {
        if (contains(s)) {
            String set = mSharedPreferences.getString(s, null);
            return new ArrayList<String>(Arrays.asList(set.substring(1, set.length() - 1).split(", ")));
        } else {
            return null;
        }
    }

    public List<String> getList(String s) {
        if (contains(s)) {
            String set = mSharedPreferences.getString(s, null);
            return Arrays.asList(set.substring(1, set.length() - 1).split(", "));
        } else {
            return null;
        }
    }

    public boolean contains(String s) {
        return mSharedPreferences.contains(s);
    }
}
