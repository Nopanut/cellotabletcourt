package net.pirsquare.cello.court.utils;

import android.content.Context;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Nut on 6/29/2016.
 */
public class ToastDialog {
    private Context context = null;
    Toast toast = null;
    Toast mToast;

    public ToastDialog(Context context) {
        this.context = context;
    }

    public void showToast(String string) {
        try {
            toast.getView().isShown();
            toast.setText(string);

        } catch (Exception e) {
            toast = Toast.makeText(context, string, Toast.LENGTH_SHORT);
        }
        toast.show();

    }
    public void showToastNew(String message) {
        if (mToast != null) {
            mToast.cancel();
            mToast = null;
        }
        mToast = Toast.makeText(context, message, Toast.LENGTH_SHORT);
        mToast.show();
    }
}
