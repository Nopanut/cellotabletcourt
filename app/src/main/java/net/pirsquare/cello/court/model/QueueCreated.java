package net.pirsquare.cello.court.model;

import java.util.List;

/**
 * Created by Nut on 12/19/16.
 */

public class QueueCreated {

    /**
     * _id : wDnmhEbsjyntLhyqF
     * service : {"id":"44po5qLup9","name":"รับฟ้องอายา"}
     * status : accepted
     * refCode : AmjujoD
     * actions : ["accepted"]
     * createdAt : {"$date":1482142998125}
     * remain : 5
     * group : C
     * index : 15
     * user : {"providers":{}}
     * capacity : 1
     * extras : {}
     */

    private String _id;
    private ServiceBean service;
    private String status;
    private String refCode;
    private CreatedAtBean createdAt;
    private int remain;
    private String group;
    private String index;
    private UserBean user;
    private int capacity;
    private ExtrasBean extras;
    private List<String> actions;

    public String get_id() {
        return _id;
    }

    public ServiceBean getService() {
        return service;
    }

    public String getStatus() {
        return status;
    }

    public String getRefCode() {
        return refCode;
    }

    public CreatedAtBean getCreatedAt() {
        return createdAt;
    }

    public int getRemain() {
        return remain;
    }

    public String getGroup() {
        return group;
    }

    public String getIndex() {
        return index;
    }

    public UserBean getUser() {
        return user;
    }

    public int getCapacity() {
        return capacity;
    }

    public ExtrasBean getExtras() {
        return extras;
    }

    public List<String> getActions() {
        return actions;
    }

    public static class ServiceBean {
        /**
         * id : 44po5qLup9
         * name : รับฟ้องอายา
         */

        private String id;
        private String name;

        public String getId() {
            return id;
        }

        public String getName() {
            return name;
        }

    }

    public static class CreatedAtBean {
        /**
         * $date : 1482142998125
         */

        private long $date;

        public long get$date() {
            return $date;
        }

    }

    public static class UserBean {
        /**
         * providers : {}
         */

        private ProvidersBean providers;

        public ProvidersBean getProviders() {
            return providers;
        }

        public static class ProvidersBean {
        }
    }

    public static class ExtrasBean {
    }
}
