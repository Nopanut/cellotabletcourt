package net.pirsquare.cello.court.utils;

/**
 * Created by kampolleelaporn on 3/29/16.
 */
public class Constant {

    public static final String METEOR_SERVER_URL = "ws://192.168.2.109:4343/websocket";
    public static final String API_QR_URL = "https://d1saoc02k62vdj.cloudfront.net/qr/";


    public static final String[] FLOORS = {"2F", "3F", "4 - 6", "ไม่เลือก"};

}