package net.pirsquare.cello.court.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.SharedPreferences;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.github.kittinunf.fuel.Fuel;
import com.github.kittinunf.fuel.core.FuelError;
import com.github.kittinunf.fuel.core.Request;
import com.google.gson.reflect.TypeToken;
import com.orhanobut.logger.Logger;

import net.pirsquare.cello.court.R;
import net.pirsquare.cello.court.adapter.FragmentPagerAdapter;
import net.pirsquare.cello.court.adapter.ServiceAdapter;
import net.pirsquare.cello.court.adapter.ServiceSetupAdapter;
import net.pirsquare.cello.court.api.GsonHelper;
import net.pirsquare.cello.court.manager.BranchManager;
import net.pirsquare.cello.court.manager.DDPManager;
import net.pirsquare.cello.court.manager.PrinterManager;
import net.pirsquare.cello.court.manager.RxBus;
import net.pirsquare.cello.court.manager.ServiceManager;
import net.pirsquare.cello.court.model.BranchDataConfig;
import net.pirsquare.cello.court.model.ServiceWithId;
import net.pirsquare.cello.court.utils.ProgressDialogBox;
import net.pirsquare.cello.court.utils.ToastDialog;
import net.pirsquare.cello.court.view.CustomSquareLayout;
import net.pirsquare.cello.court.view.NonSwipeableViewPager;
import net.pirsquare.cello.court.view.SlidingTabLayout;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

import static net.pirsquare.cello.court.utils.Utils.validIP;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Observable<Boolean> observableMeteorStatus;
    private RxBus rxBus = null;
    private CompositeSubscription subscriptions;

    private boolean isReconnect;
    private boolean initPrinter;
    private boolean exit = false;
    private boolean serverOn, printerOn;

    public ToastDialog toastDialog;
    public ProgressDialogBox dialogBox;
    private Button btnRecon, btnSetIp, btnReset;
    private CustomSquareLayout circleServer, circlePrinter;
    private TextView tvIp;
    private String ipPref, servicePref;
    private Switch switchAutoPrint;

    @BindView(R.id.viewPager)
    NonSwipeableViewPager viewPager;
    @BindView(R.id.slidingTabLayout)
    SlidingTabLayout slidingTabLayout;
    @BindView(R.id.layout_ss)
    FrameLayout layoutSplashScreen;
    @BindView(R.id.layout_setting)
    FrameLayout layoutSetting;
    @BindView(R.id.firstFloor)
    Button btnFirstFloor;
    @BindView(R.id.secFloor)
    Button btnSecFloor;
    @BindView(R.id.thirdFloor)
    Button btnThirdFloor;
    @BindView(R.id.circle_status)
    CustomSquareLayout circleStatus;
    @BindView(R.id.circle_area)
    RelativeLayout circleArea;

    @Override
    protected void onResume() {
        super.onResume();
        subscriptions = new CompositeSubscription();
        subscriptions
                .add(rxBus.toObserverable()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(event -> {
                            try {
                                if (event instanceof PrinterManager.PrinterEvent) {
                                    PrinterManager.PrinterEvent printerEvent = (PrinterManager.PrinterEvent) event;
                                    Log.e("Printer Status", "Status " + (printerEvent.isConnect ? "Connect" : "Disconnect"));
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            if (printerEvent.isConnect) {
                                                printerOn = true;
                                                circleStatus.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.circle_background_green, null));
                                            } else {
                                                printerOn = false;
                                                circleStatus.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.circle_background_red, null));
                                            }
                                            PrinterManager.getInstance().setReady(printerEvent.isConnect);
                                        }
                                    });

                                }
                            } catch (Exception e1) {
                                Log.e("ERROR", e1.getMessage());
                            }

                        }, new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                Log.e("ERROR", throwable.getMessage());
                            }
                        }));
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.AppTheme);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        toastDialog = new ToastDialog(this);
        dialogBox = new ProgressDialogBox(this);
        rxBus = RxBus.getInstance();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        connectMeteor();
    }


    private void connectMeteor() {
        if (DDPManager.getInstance() == null || !DDPManager.getInstance().isConnect())
            Observable.timer(2000, TimeUnit.MILLISECONDS)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(aLong -> {
                        DDPManager.getInstance().init(MainActivity.this, new DDPManager.ConnectServerListener() {
                            @Override
                            public void onSuccess() {
                                ServiceManager.getInstance().setHadService(true);
                                ServiceManager.getInstance().backupService();

                                setTabLayout();
                                setupPrinter();

                                layoutSplashScreen.animate().alpha(0.0f).setDuration(1200).setListener(new AnimatorListenerAdapter() {
                                    @Override
                                    public void onAnimationEnd(Animator animation) {
                                        super.onAnimationEnd(animation);
                                        layoutSplashScreen.setVisibility(View.GONE);
                                    }
                                }).start();
                                ServiceManager.getInstance().setHadService(true);

                                setObservableStatus();
                            }

                            @Override
                            public void onFail() {
                                reLoginMeteor();
                            }
                        });
                    });
    }

    private void setObservableStatus() {
        observableMeteorStatus = DDPManager.getInstance().subscribeStatus();
        observableMeteorStatus.delaySubscription(5000, TimeUnit.MILLISECONDS).subscribe(new Action1<Boolean>() {
            @Override
            public void call(Boolean aBoolean) {
                if (!aBoolean && !isReconnect) {
                    Log.e("Meteor", "Reconnect......");
                    isReconnect = true;
                    circleStatus.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.circle_background_red, null));

                    DDPManager.getInstance().onReconnect(new DDPManager.ReconnectListener() {
                        @Override
                        public void reconnectSuccess() {
                            isReconnect = false;
                            serverOn = true;
                            Logger.e("Reconnect Success");
                            toastDialog.showToast("Reconnect Success");
                            if (printerOn)
                                circleStatus.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.circle_background_green, null));
                        }

                        @Override
                        public void reconnectFail() {
                            isReconnect = false;
                            serverOn = false;
                            Logger.e("Reconnect Fail");
                            circleStatus.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.circle_background_red, null));
                        }
                    });
                    Logger.e("aBoolean : " + aBoolean + " isReconnect : " + isReconnect + " waiting server reconnect");
                }
            }
        });
    }

    private void reLoginMeteor() {
        if (!isReconnect) {
            Log.e("Meteor", "Reconnect......");
            isReconnect = true;
            Observable.timer(3000, TimeUnit.MILLISECONDS)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Action1<Long>() {
                        @Override
                        public void call(Long aLong) {
                            DDPManager.getInstance().onReconnect(new DDPManager.ReconnectListener() {
                                @Override
                                public void reconnectSuccess() {
                                    isReconnect = false;
                                    Logger.e("Reconnect Success");
                                    if (printerOn)
                                        circleStatus.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.circle_background_green, null));
                                }

                                @Override
                                public void reconnectFail() {
                                    isReconnect = false;
                                    Logger.e("Reconnect Fail");
                                    MaterialDialog.Builder builder = new MaterialDialog.Builder(MainActivity.this)
                                            .title(R.string.disconnect)
                                            .content(R.string.disconnect_msg)
                                            .positiveText(R.string.ok)
                                            .positiveColorRes(R.color.color_black)
                                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                                @Override
                                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                    dialog.dismiss();
                                                    onDestroy();
                                                    MainActivity.this.finishAffinity();
                                                    System.exit(1);
                                                }
                                            })
                                            .cancelable(false)
                                            .autoDismiss(false);
                                    MaterialDialog dialog = builder.build();
                                    dialog.show();

                                }
                            });
                        }
                    });
        }
    }

    private void setTabLayout() {
        SharedPreferences sharedPref = getPreferences(MODE_PRIVATE);
        servicePref = sharedPref.getString("services", "");
        Log.e("SetupService", servicePref);
        if (TextUtils.isEmpty(servicePref)) {
            layoutSetting.setVisibility(View.VISIBLE);
            setServiceToFloor();
        } else {
            Type listType = new TypeToken<ArrayList<ServiceWithId>>() {
            }.getType();
            ArrayList<ServiceWithId> list = GsonHelper.getGson().fromJson(servicePref, listType);
            ServiceManager.getInstance().updateServiceWithId(list);
            setServicePager();
        }

        serverOn = true;
        circleStatus.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.circle_background_green, null));


        btnFirstFloor.setOnClickListener(this);
        btnSecFloor.setOnClickListener(this);
        btnThirdFloor.setOnClickListener(this);
        circleArea.setOnClickListener(this);
    }

    private void setServicePager() {
        viewPager.setAdapter(new FragmentPagerAdapter(getSupportFragmentManager()));
        viewPager.setOffscreenPageLimit(3);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        btnFirstFloor.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.left_bt_active, null));
                        btnFirstFloor.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.color_white));
                        btnSecFloor.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.middle_bt_normal, null));
                        btnSecFloor.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.color_black));
                        btnThirdFloor.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.right_bt_normal, null));
                        btnThirdFloor.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.color_black));
                        break;
                    case 1:
                        btnFirstFloor.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.left_bt_normal, null));
                        btnFirstFloor.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.color_black));
                        btnSecFloor.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.middle_bt_active, null));
                        btnSecFloor.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.color_white));
                        btnThirdFloor.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.right_bt_normal, null));
                        btnThirdFloor.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.color_black));
                        break;
                    case 2:
                        btnFirstFloor.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.left_bt_normal, null));
                        btnFirstFloor.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.color_black));
                        btnSecFloor.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.middle_bt_normal, null));
                        btnSecFloor.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.color_black));
                        btnThirdFloor.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.right_bt_active, null));
                        btnThirdFloor.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.color_white));
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

    }

    private void setServiceToFloor() {
        MaterialDialog dialog = new MaterialDialog.Builder(this)
                .title(R.string.service_setting)
                .customView(R.layout.service_setup, false)
                .positiveText(R.string.done)
                .positiveColorRes(R.color.color_black)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        setServicePager();

                        SharedPreferences sharedPref = getPreferences(MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putString("services", GsonHelper.getGson().toJson(ServiceManager.getInstance().getServiceWithIds()));
                        editor.commit();

                        dialog.dismiss();
                    }
                })
                .cancelable(false)
                .build();

        RecyclerView mRecyclerView;
        RecyclerView.LayoutManager mLayoutManager;
        ServiceSetupAdapter adapter;
        Button btnDone;

        btnDone = (Button) dialog.getCustomView().findViewById(R.id.btn_done);
        mRecyclerView = (RecyclerView) dialog.getCustomView().findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(MainActivity.this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        adapter = new ServiceSetupAdapter(ServiceManager.getInstance().getServiceWithIds(), MainActivity.this);
        mRecyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ServiceManager.getInstance().updateServiceWithId(adapter.getListService());

                setServicePager();

                SharedPreferences sharedPref = getPreferences(MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString("services", GsonHelper.getGson().toJson(ServiceManager.getInstance().getServiceWithIds()));
                editor.apply();
                layoutSetting.animate().alpha(0.0f).setDuration(750).setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        layoutSetting.setVisibility(View.GONE);
                    }
                }).start();
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void setupPrinter() {
        if (!initPrinter) {
            initPrinter = true;

            if (!TextUtils.isEmpty(BranchManager.getInstance().getBranchDataConfig().getConfigs().getPrinters().getLogoTop()))
                Fuel.download(BranchManager.getInstance().getBranchDataConfig().getConfigs().getPrinters().getLogoTop()).destination((response1, url) -> {
                    File sd = Environment.getExternalStorageDirectory();
                    return new File(sd, "logo_top.jpg");
                }).responseString(new com.github.kittinunf.fuel.core.Handler<String>() {
                    @Override
                    public void success(@NotNull Request request, @NotNull com.github.kittinunf.fuel.core.Response response, String s) {

                    }

                    @Override
                    public void failure(@NotNull Request request, @NotNull com.github.kittinunf.fuel.core.Response response, @NotNull FuelError fuelError) {

                    }
                });

            if (!TextUtils.isEmpty(BranchManager.getInstance().getBranchDataConfig().getConfigs().getPrinters().getLogoBottom()))
                Fuel.download(BranchManager.getInstance().getBranchDataConfig().getConfigs().getPrinters().getLogoBottom()).destination((response1, url) -> {
                    File sd = Environment.getExternalStorageDirectory();
                    return new File(sd, "logo_bottom.jpg");
                }).responseString(new com.github.kittinunf.fuel.core.Handler<String>() {
                    @Override
                    public void success(@NotNull Request request, @NotNull com.github.kittinunf.fuel.core.Response response, String s) {

                    }

                    @Override
                    public void failure(@NotNull Request request, @NotNull com.github.kittinunf.fuel.core.Response response, @NotNull FuelError fuelError) {

                    }
                });

            SharedPreferences sharedPref = getPreferences(MODE_PRIVATE);
            ipPref = sharedPref.getString("ip", "");
            Log.e("SetupPrinter", ipPref);
            if (TextUtils.isEmpty(ipPref)) {
                openSetIPDialog();
            } else {
                setIpPrinter(ipPref);
            }
        }
    }

    private void setIpPrinter(String ip) {
        PrinterManager.getInstance().init(ip, this);
        PrinterManager.getInstance().setAutoPrint(true);
        PrinterManager.getInstance().setReady(true);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.firstFloor:
                viewPager.setCurrentItem(0);
                break;
            case R.id.secFloor:
                viewPager.setCurrentItem(1);
                break;
            case R.id.thirdFloor:
                viewPager.setCurrentItem(2);
                break;
            case R.id.circle_area:
                settingDialog();
                break;
        }
    }

    private void settingDialog() {

        MaterialDialog dialog = new MaterialDialog.Builder(this)
                .title(R.string.setting)
                .customView(R.layout.dialog_customview, true)
                .positiveText(R.string.done)
                .positiveColorRes(R.color.color_black)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .cancelable(false)
                .build();

        tvIp = (TextView) dialog.getCustomView().findViewById(R.id.tv_printer_ip);
        tvIp.setText(String.format("Printer Ip Address : %s", ipPref));

        switchAutoPrint = (Switch) dialog.getCustomView().findViewById(R.id.checkbox_auto_print);
        switchAutoPrint.setChecked(PrinterManager.getInstance().isAutoPrint());
        switchAutoPrint.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                PrinterManager.getInstance().setAutoPrint(isChecked);
            }
        });
        circleServer = (CustomSquareLayout) dialog.getCustomView().findViewById(R.id.circle_server);
        circlePrinter = (CustomSquareLayout) dialog.getCustomView().findViewById(R.id.circle_printer);
        if (serverOn)
            circleServer.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.circle_background_green, null));
        else
            circleStatus.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.circle_background_red, null));

        if (printerOn)
            circlePrinter.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.circle_background_green, null));
        else
            circlePrinter.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.circle_background_red, null));

        btnRecon = (Button) dialog.getCustomView().findViewById(R.id.btnRecon);
        btnSetIp = (Button) dialog.getCustomView().findViewById(R.id.btnSetIp);
        btnReset = (Button) dialog.getCustomView().findViewById(R.id.btnReset);

        btnRecon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                PrinterManager.getInstance().checkPrint(new PrinterManager.ConnectPrinterListener() {
                    @Override
                    public void onConnectSuccess() {
                        Log.e("print", "Printer : onConnectSuccess");
                    }

                    @Override
                    public void onConnectFail() {
                        Log.e("print", "Printer : onConnectFail");
                    }
                });
            }
        });
        btnSetIp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                openSetIPDialog();
            }
        });
        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedPref = getPreferences(MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString("services", "");
                editor.commit();
                dialog.dismiss();
                Toast.makeText(MainActivity.this, "please restart application", Toast.LENGTH_LONG).show();

            }
        });

        dialog.show();
    }

    private void openSetIPDialog() {
        MaterialDialog.Builder builder = new MaterialDialog.Builder(this)
                .title("Set IP ")
                .inputType(InputType.TYPE_CLASS_TEXT)
                .input("set here", null, new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(MaterialDialog dialog, CharSequence input) {

                        if (input.toString().startsWith("TCP:")) {

                            String ipCut = (String) input.toString().subSequence(4, input.length());
                            Log.e("ipCut", "ipCut = " + ipCut);

                            if (validIP(ipCut)) {

                                SharedPreferences sharedPref = getPreferences(MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedPref.edit();
                                editor.putString("ip", input.toString());
                                editor.commit();

                                setIpPrinter(input.toString());
                                dialog.dismiss();
                                Log.e("openSetIPDialog", input.toString());
                            } else {
                                toastDialog.showToast("ValidIP");
                            }
                        } else {
                            toastDialog.showToast("Please Start with TCP:");
                        }
                    }
                })
                .positiveColorRes(R.color.colorPrimary)
                .buttonRippleColorRes(R.color.colorPrimary)
                .autoDismiss(false)
                .cancelable(false);
        MaterialDialog dialog = builder.build();
        dialog.show();


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        DDPManager.getInstance().close();
        subscriptions.unsubscribe();
    }

    @Override
    public void onBackPressed() {
        if (exit) {
            super.finishAffinity();

        } else {
            Toast.makeText(this, "Press back again to exit.", Toast.LENGTH_SHORT).show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 2 * 1000);
        }
    }

}
