package net.pirsquare.cello.court.manager;

import android.content.Context;

import net.pirsquare.cello.court.model.ServiceWithId;

import java.util.ArrayList;


public class ServiceManager {

    private ArrayList<ServiceWithId> serviceWithIds;
    private ArrayList<ServiceWithId> serviceWithIdsBackUp;
    private boolean hadService;

    private static ServiceManager instance;

    public static ServiceManager getInstance() {
        if (instance == null)
            instance = new ServiceManager();
        return instance;
    }

    private Context mContext;

    private ServiceManager() {
        mContext = Contextor.getInstance().getContext();
        serviceWithIds = new ArrayList<>();
        serviceWithIdsBackUp = new ArrayList<>();
    }

    public ArrayList<ServiceWithId> getServiceWithIds() {
        return serviceWithIds;
    }

    public void addServiceWithIds(ServiceWithId serviceWithIds) {
        this.serviceWithIds.add(serviceWithIds);
    }

    public void backupService() {
        this.serviceWithIdsBackUp.addAll(this.serviceWithIds);
    }

    public void updateServiceWithId(ArrayList<ServiceWithId> serviceWithId) {
        serviceWithIds = new ArrayList<>();
        this.serviceWithIds.addAll(serviceWithId);
    }

    public boolean isHadService() {
        return hadService;
    }

    public void setHadService(boolean hadService) {
        this.hadService = hadService;
    }
}
