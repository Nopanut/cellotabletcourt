package net.pirsquare.cello.court.utils;

import android.content.Context;
import android.app.ProgressDialog;
import android.support.annotation.NonNull;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import net.pirsquare.cello.court.R;
import net.pirsquare.cello.court.activity.MainActivity;

/**
 * Created by Nut on 6/29/2016.
 */
public class ProgressDialogBox {
    private MaterialDialog dialog;

    private Context context = null;

    public ProgressDialogBox(Context context) {
        this.context = context;
    }


    public void showProgressDialog(String title, String message) {
        MaterialDialog.Builder builder = new MaterialDialog.Builder(context)
                .title(title)
                .content(message)
                .progress(true, 0)
                .progressIndeterminateStyle(false)
                .cancelable(false);
        dialog = builder.build();
        dialog.show();
    }

    public void hideProgressDialog() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    public void showPrintingDialog(String title, String message) {
        MaterialDialog.Builder builder = new MaterialDialog.Builder(context)
                .title(title)
                .content(message)
                .cancelable(false);
        dialog = builder.build();
        dialog.show();
    }

    public void showMessageDialog(String title, String message) {
        MaterialDialog.Builder builder = new MaterialDialog.Builder(context)
                .title(title)
                .content(message)
                .cancelable(false)
                .positiveColorRes(R.color.color_black)
                .positiveText("ok");
        dialog = builder.build();
        dialog.show();
    }


}
