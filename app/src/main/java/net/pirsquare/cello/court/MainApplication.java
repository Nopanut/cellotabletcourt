package net.pirsquare.cello.court;

import android.app.Application;

import com.orhanobut.logger.Logger;

import net.danlew.android.joda.JodaTimeAndroid;
import net.pirsquare.cello.court.manager.Contextor;
import net.pirsquare.cello.court.manager.PrinterManager;
import net.pirsquare.cello.court.utils.ProgressDialogBox;
import net.pirsquare.cello.court.utils.ToastDialog;


/**
 * Created by Nut on 12/1/2016.
 */

public class MainApplication extends Application {

    public ToastDialog toastDialog;

    @Override
    public void onCreate() {
        super.onCreate();
        Contextor.getInstance().init(getApplicationContext());
        JodaTimeAndroid.init(this);
        toastDialog = new ToastDialog(this);
        Logger.init("court");
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }
}
