package net.pirsquare.cello.court.manager;

import android.content.Context;

import net.pirsquare.cello.court.model.BranchDataConfig;

/**
 * Created by Nut on 12/19/2016.
 */
public class BranchManager {

    private static BranchManager instance;
    private BranchDataConfig branchDataConfig;

    public static BranchManager getInstance() {
        if (instance == null)
            instance = new BranchManager();
        return instance;
    }

    private Context mContext;

    private BranchManager() {
        mContext = Contextor.getInstance().getContext();
    }

    public BranchDataConfig getBranchDataConfig() {
        return branchDataConfig;
    }

    public void setBranchDataConfig(BranchDataConfig branchDataConfig) {
        this.branchDataConfig = branchDataConfig;
    }
}
