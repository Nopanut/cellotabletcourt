package net.pirsquare.cello.court.dialog;


import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import net.pirsquare.cello.court.manager.base.FullscreenDialogFragment;

import butterknife.ButterKnife;

/**
 * Created by thanawat on 8/23/15 AD.
 * Project : MindShot.
 */
public class TemplateDialogFragmentViewController extends FullscreenDialogFragment implements View.OnClickListener {


    public static TemplateDialogFragmentViewController newInstance() {
        TemplateDialogFragmentViewController fragment = new TemplateDialogFragmentViewController();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public TemplateDialogFragmentViewController() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(net.pirsquare.cello.court.R.layout.dialog_setting, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        int animationId = getEnterAnimation();

        if (animationId != 0) {
            Animation animation = AnimationUtils.loadAnimation(view.getContext(), animationId);
            view.startAnimation(animation);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        /**
         * Add Function
         */

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        /**
         * For Add Call Back
         */
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.bind(getActivity()).unbind();
    }

    @Override
    public void onClick(View v) {

    }


}
