package net.pirsquare.cello.court.manager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

public class RestartBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(final Context context, final Intent intent) {
        final String msg = "intent:" + intent + " action:" + intent.getAction();
        Log.e("DEBUG", msg);
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }
}