package net.pirsquare.cello.court.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.orhanobut.logger.Logger;

import net.pirsquare.cello.court.R;
import net.pirsquare.cello.court.model.ServiceWithId;
import net.pirsquare.cello.court.utils.Constant;

import java.util.ArrayList;

import fr.ganfra.materialspinner.MaterialSpinner;

/**
 * Created by kung on 1/4/17.
 */

public class ServiceSetupAdapter extends RecyclerView.Adapter<ServiceSetupAdapter.MyViewHolder> {

    private ArrayList<ServiceWithId> listService;
    private ArrayAdapter<String> adapter;
    private Context context;

    public ServiceSetupAdapter(ArrayList<ServiceWithId> myDataset, Context context) {
        this.listService = myDataset;
        this.context = context;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        final TextView tvService;
        final MaterialSpinner spinner;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvService = (TextView) itemView.findViewById(R.id.ss_service_name);
            spinner = (MaterialSpinner) itemView.findViewById(R.id.ss_spinner);

        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_service_setup, parent, false);
        return new MyViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.tvService.setText(listService.get(position).getService().getName());
        adapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, Constant.FLOORS);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        holder.spinner.setAdapter(adapter);
        holder.spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int spinnerPosition, long id) {
                listService.get(position).setFloor((holder.spinner.getSelectedItemPosition() + 1));
//                Logger.e((holder.spinner.getSelectedItemPosition() + 1) + "");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return listService.size();
    }

    public ArrayList<ServiceWithId> getListService() {
        return listService;
    }
}
