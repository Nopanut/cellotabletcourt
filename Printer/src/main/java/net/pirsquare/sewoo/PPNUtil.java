//package net.pirsquare.sewoo;
//
//import java.util.HashMap;
//import java.util.Iterator;
//import java.util.Set;
//
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import android.bluetooth.BluetoothAdapter;
//import android.bluetooth.BluetoothDevice;
//import android.content.Context;
//import android.net.wifi.WifiInfo;
//import android.net.wifi.WifiManager;
//import android.util.Base64;
//import android.util.Log;
//
//public class PPNUtil
//{
//	private static BluetoothAdapter _MYBTADAPTER;
//
//	public static void setDeviceName(String deviceName)
//	{
//		try
//		{
//		    BluetoothAdapter.getDefaultAdapter().setName(deviceName);
//		} catch (Exception e)
//		{
//			Log.e("PPN", e.getMessage());
//		}
//	}
//	
//	public static String getMacAddress(Context context)
//	{
//		String _MacAddress = "";
//		try
//		{
//			WifiManager _WifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
//			WifiInfo _WifiInfo = _WifiManager.getConnectionInfo();
//			_MacAddress = _WifiInfo.getMacAddress();
//		} catch (Exception e)
//		{
//			Log.e("PPN", e.getMessage());
//		}
//
//		return _MacAddress;
//	}
//	
//	public static String getDeviceNameAndAddress()
//	{
//		String deviceName = "";
//		String deviceAddress = "";
//
//		try
//		{
//			BluetoothAdapter myDevice = BluetoothAdapter.getDefaultAdapter();
//			deviceName = myDevice.getAddress();
//			deviceAddress = myDevice.getAddress();
//		} catch (Exception e)
//		{
//			Log.e("PPN", e.getMessage());
//		}
//
//		return deviceName+","+deviceAddress;
//	}
//	
//	public static String getBluetoothAddress()
//	{
//		String deviceAddress = "";
//
//		try
//		{
//			if(_MYBTADAPTER==null){
//				_MYBTADAPTER = BluetoothAdapter.getDefaultAdapter();
//			}
//			deviceAddress = _MYBTADAPTER.getAddress();
//		} catch (Exception e)
//		{
//			Log.e("PPN", e.getMessage());
//		}
//
//		return deviceAddress;
//	}
//	
//	public static String[] getPairBluetoothAddress()
//	{
//		String[] resultAddress= {"empty","empty"};
//  
//		try
//		{
//			if(_MYBTADAPTER==null)
//				_MYBTADAPTER = BluetoothAdapter.getDefaultAdapter();
//			
//			Set<BluetoothDevice> pairedDevices = _MYBTADAPTER.getBondedDevices();
//
//			int pairedDevicessize = pairedDevices.size();
//			String currentAddress;
//	        if ( pairedDevicessize == 2||pairedDevicessize == 1) {
//	            for (BluetoothDevice device : pairedDevices) {
//	            	
//	            	currentAddress = device.getAddress();
//	            	
//	            	Boolean isPrinter = currentAddress.indexOf("00:13:7B") == 0;
//
//	            	if(isPrinter)
//	            		resultAddress[0]=currentAddress;
//	            	else
//	            		resultAddress[1]=currentAddress;
//	            }
//	        }else{
//	        	resultAddress = null;
//	        }
//	        
//		} catch (Exception e)
//		{
//			Log.e("PPN", e.getMessage());
//		}
//
//		return resultAddress;
//	}
//	
//	public static String getDeviceName()
//	{
//		String deviceName = "";
//
//		try
//		{
//			BluetoothAdapter myDevice = BluetoothAdapter.getDefaultAdapter();
//			if(!myDevice.isEnabled()){
//				myDevice.enable();
//			}			
//			deviceName = myDevice.getName();
//		} catch (Exception e)
//		{
//			Log.e("PPN", e.getMessage());
//		}
//
//		return deviceName;
//	}
//
//	public static String output(String result)
//	{
//		return output(result, -1,"");
//	}
//
//	public static String output(String result, int processID,String functionName)
//	{
//		//Log.i("PPN", "output before: "+result);
//		result = Base64.encodeToString(result.getBytes(), Base64.NO_WRAP);
//		//Log.i("PPN", "output after: "+result);
//		if(functionName=="")
//			return "{\"id\":" + processID + ", \"result\":\"" + result + "\"}";
//		else
//			return "{\"id\":" + processID + ", \"result\":\"" + result + "\", \"functionName\":\"" + functionName + "\"}";
//	}
//
//	public static HashMap<String, Object> getHashMapFromJSONString(String jsonString)
//	{
//		HashMap<String, Object> hashMap = new HashMap<String, Object>();
//
//		JSONObject json = null;
//
//		try
//		{
//			json = new JSONObject(jsonString);
//		} catch (JSONException e)
//		{
//			e.printStackTrace();
//		}
//
//		if (json != null)
//		{
//			Iterator<String> iter = json.keys();
//			while (iter.hasNext())
//			{
//				String key = iter.next();
//				try
//				{
//					Object value = json.get(key);
//					if (value instanceof Number)
//					{
//						hashMap.put(key, (Number) value);
//					} else if (value instanceof Boolean)
//					{
//						hashMap.put(key, (Boolean) value);
//					} else
//					{
//						hashMap.put(key, value.toString());
//					}
//
//					// TODO : Array, ParseObject
//
//				} catch (JSONException e)
//				{
//					e.printStackTrace();
//				}
//			}
//		}
//
//		return hashMap;
//	}
//
////	public static ParseObject getParseObjectFromJSONString(String className, String jsonString)
////	{
////		ParseObject parseObject = new ParseObject(className);
////
////		JSONObject json = null;
////
////		try
////		{
////			json = new JSONObject(jsonString);
////		} catch (JSONException e)
////		{
////			e.printStackTrace();
////		}
////
////		if (json != null)
////		{
////			Iterator<String> iter = json.keys();
////
////			while (iter.hasNext())
////			{
////				String key = iter.next();
////				try
////				{
////					Object value = json.get(key);
////					if (value instanceof Number)
////					{
////						parseObject.put(key, (Number) value);
////					} else if (value instanceof Boolean)
////					{
////						parseObject.put(key, (Boolean) value);
////					} else
////					{
////						parseObject.put(key, value.toString());
////					}
////
////					// TODO : Array, ParseObject
////
////				} catch (JSONException e)
////				{
////					e.printStackTrace();
////				}
////			}
////		}
////
////		return parseObject;
////	}
// }
